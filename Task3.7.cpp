// Task3.7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>
int main(int argc, char *argv[])
{
  const int n = 255;
  char str[n+1];        // строка
  char symbols[n];     // строка символов без повторений
  double chance[n];   // частота встречаемости
 
  puts("Str");
  gets(str);
  int strLenth = strlen(str);
  int symbolsLenth = 0;
 
  for(int i=0; i<strLenth; i++){
    int flg = 0;
    for(int j=0; j<symbolsLenth; j++){
      if(str[i] == symbols[j]){
        chance[j]++;
        flg = 1;
        break;}
    }
    if(!flg){
      chance[symbolsLenth] = 1.0;
      symbols[symbolsLenth] = str[i];
      symbolsLenth++;}
  }
 
  // сортировка
  int flg = 1;
  while(flg){
    flg = 0;
    for(int i=0; i<symbolsLenth-1; i++){
      if(chance[i] < chance[i+1]){
        double tmp = chance[i];
        chance[i] = chance[i+1];
        chance[i+1] = tmp;
        char ch = symbols[i];
        symbols[i] = symbols[i+1];
        symbols[i+1] = ch;
        flg = 1;}
    }
  }
 
  for(int i=0; i<symbolsLenth; i++){
    chance[i] /= strLenth;
    printf("%c - %.3lf\n", symbols[i], chance[i]);}
  return 0;
} 

