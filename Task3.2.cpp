// Task3.2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <iostream>
#define LENGTH 80
 
int main(void) 
{  
    int wordCounter = 0,longworld = 0;
    char str[LENGTH] = { 0 };
    printf("Str: ");
    fgets(str, LENGTH , stdin);
    char* ptr; 
    ptr = strtok(str, " "); 
 
    while(ptr!= NULL) 
    {  
        int tok_length = strlen(ptr);
        longworld = tok_length;
        printf("%s-%d\n", ptr,longworld); 
        ptr = strtok(NULL, " ");
    }
    for(char * p = str; *p; ++p)
    {
        if(isalnum(*p))
        {
            wordCounter++;
            while(!ispunct(*p) && !isspace(*p) && *p) ++p;	
        }
    }
    printf("Count of words : %d\n", wordCounter);
}

