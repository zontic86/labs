// Task3.9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <string.h>


#define N 256

void chomp(char *str)
{
	if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
}

int main()


{
    char str[N];
    char temp;
    char maxel;
    int i=0;
    int max=0;
    
    int count = 1;
    
    puts("Please enter a string:");
    fgets(str,N,stdin);
    chomp(str);
    
    
    for(i=0;i<strlen(str)+1;i++)
    {
        if (str[i]==str[i+1])
        {
            temp=str[i];
            count++;
        }
        
        else
        {
            
        	if(count>max)
            {
                max=count;
        		maxel=temp;
        		count=1;
            }
            
        }
        
    }
    
    
    
    printf("The longest string of characters contains %d characters %c", max, maxel);
    return 0;
}


