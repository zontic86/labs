// Task3.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

#define ARRAY_SIZE 80

int _tmain(int argc, _TCHAR* argv[])
{
    char userStr[ARRAY_SIZE] = { 0 };
    int wordCounter = 0;
    printf("userStr:\n");
    fgets(userStr, ARRAY_SIZE, stdin);

    for(char * p = userStr; *p; ++p)
    {
        if(isalnum(*p))
        {
            wordCounter++;
            while(!ispunct(*p) && !isspace(*p) && *p) ++p;	
            //ispunct ������ ������ ���� ������ ���� ����������
            //isspace  ������ ������ ���� ������ ������
            //issalnum -  ������ ������ ���� ������ �� ����� ��� �����, �� �� ������� ��� ���� ����������
        }
    
    }
    printf("Count of words : %d\n", wordCounter);
    system("PAUSE");
    return 0;
}

