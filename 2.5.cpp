// 2.5.cpp : Defines the entry point for the console application.
//
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main()
{
	int i,j;
	srand(time(0));	
	for(i=1;i<=10;i++)
	{
		for(j=0;j<8;j++)
		{
			int k = rand()%3;
			switch(k)
			{
			case 0:
				putchar(rand()%('Z'-'A'+1)+'A');
				break;
			case 1:
				putchar(rand()%('z'-'a'+1)+'a');
				break;
			case 2:
				putchar(rand()%('9'-'0'+1)+'0');
				break;
			}
		}
		putchar('\n');
	}
			
	return 0;
}
