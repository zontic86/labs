// 2.4.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>


void change(int m1, int m2, char str[])
{
	char tmp;
	tmp=str[m2];
	str[m2]=str[m1];
	str[m1]=tmp;
}

int main()
{
	int m1,m2,line;
	char tmp;
	char str[]="DFGNM255CK4VJ7GGDG6";
	printf("%s\n", str);	
	line=strlen(str);
	for(m1=0,m2=line-1;m1<m2;m1++,m2--)
	{
		if(str[m1] >='0' && str[m1] <='9')
		{
			while(!(str[m2] >='A' && str[m2] <= 'Z'))
			{
				m2--;
			}
			change(m1,m2,str);
		}
		else if((str[m2] >= 'A') && (str[m2] <= 'Z'))
		{
			while(!(str[m1] >= '0' && str[m1] <= '9'))
			{
				m1++;
			}
			change(m1,m2,str);
		}		
	}
	printf("%s\n", str);
	return 0;
}
