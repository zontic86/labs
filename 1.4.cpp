// 1.4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <stdio.h>
 
#define FT 12 * 2.54
#define INCH 2.54



int _tmain(int argc, _TCHAR* argv[])
{
    int input_ft = 0, input_inch = 0;
    float ft_in_cm = 0.0, inch_in_cm = 0.0, growth = 0.0;
    int res = 1;
    printf("Enter value ft,inch:\n");
    while(res > 0)
	{
    res = scanf("%i,%f", &input_ft,&input_inch);
    if(input_ft > 0 && input_inch > 0)
	{
        ft_in_cm = input_ft * FT;
        inch_in_cm = input_inch * INCH;
        growth = ft_in_cm + inch_in_cm;
        printf("%.1f growth \n", growth);
        break;
    }
        else if(res != 1 && input_ft < 0 && input_inch < 0)
		{
            printf("Enter value ft,inch:\n");
            flushall();
            res = res + 1;
        }
        }
    return 0;
}

