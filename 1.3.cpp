// 1.3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define PI 3.14159265

int _tmain(int argc, _TCHAR* argv[])
{

    float degrees = 0.0, radians = 0.0, value = 0.0;
    char item_kind = 0;
    int res = 1;
    printf("Enter value:\n");
    
    while(res > 0)
	{
		res = scanf("%f%c", &value, &item_kind);
		if(res == 2)
	{
   
        if (item_kind == 'R')
		{
          degrees = value * (180.0 / PI);
          printf("%f degrees \n", degrees);
          break;
        }
        else if (item_kind == 'D')
		{
          radians = value * (PI / 180.0);
          printf("%f radians \n", radians);
          break;
        }
        else if (res != 1)
		{
          printf("Enter value:\n");
          flushall();
          res = res + 1;
        }
        }
    else
	{
        char choise = 0;
        printf("Input is incorrect. Y - one more attempt. Q - quite");
        scanf("%c", &choise);
    if(choise == 'Q')
	{
        res = 0;
    }
    }
    }
 return 0;
}